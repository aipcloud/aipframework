AIPHelloWorld is a sample Unity project.

To Run the project

1. Download the entire AIPHelloWorld folder to your local system
2. Open Unity, Open Project--> select the AIPHelloWorld as the source 
3. After Unity is done its inital build, change the resolution of the "Game View" 
   from standard(16:9) to custom 1920x1080
4. Hit the play button to run 