﻿using UnityEngine;
using AIPFramework;
using System.Collections.Generic;
using SimpleJson; 

public class StartGame : AIPEventListener 
{
	public Dictionary<string,string> testDictionary = new Dictionary<string,string> ();  
	public AIPNetwork myNetwork = new AIPNetwork(); 
	public string currentMessage = "No message yet"; 

	//AIPEventListener is inherited from MonoBehaviour, so methods such as
	//Start(), OnGUI(), OnApplicationPause(), and OnApplicationQuit() can be
	//called. It is better design to have a "gameController.cs" script separate
	//from your listeners that controls all connections and game behaviour,
	//however this example is relatively simple, and has been condensed to one
	//class. 


	
	// Use this for initialization
	void Start () 
	{
		//Create custom network object "myNetwork", connect
		myNetwork.Connect();

		//View the exposed values in AIPNetwork 
		//Note that myNetwork.isConnected may return a false value as the 
		//connection occurs in a separate thread and is time dependant 

		Debug.Log ("myNetwork.Mobicode: " + myNetwork.Mobicode); 
		//Debug.Log ("myNetwork.isConnected: " + myNetwork.isConnected);	
	}

	void OnGUI()
	{
		//Display the Mobicode and most recent message to the user
		GUI.Box(new Rect(5,5,400,40),"Mobicode: "+myNetwork.Mobicode); 
		GUI.Box(new Rect(5,50,400,40), currentMessage); 		
	}

	//Display the contents of any messages received from clients
	public override void eventMessage(string name, string data, string clientId)
	{
		Debug.Log ("We received a message from a client: " + data); 
		Debug.Log("Its name is: "+name);
		Debug.Log("It came from: "+clientId); 

		//If a client has sent a "helloevent", respond to that specific client 
		if(name=="helloevent"){
			currentMessage = data;  
			testDictionary.Clear ();
			testDictionary.Add ("hellobackmessage","The server says hello"); 
			myNetwork.Send("helloback",testDictionary,clientId);   
		}
	}

	void OnApplicationFocus(){
		//This call forces the APP to enter fullscreen mode (hiding the status and navigation bars)
		DisableSystemUI.DisableNavUI (); 
	}

	//When the application is paused, force it to quit. This will call the
	//OnApplicationQuit() method. 
	void OnApplicationPause(bool pauseStatus) {
		Application.Quit ();
	}
	
	//When the application quits, close all open connections. This will usually
	//only be the AIPNetwork object that you have created. Improperly, or 
	//neglecting to close connections will cause Unity to crash. 
	void OnApplicationQuit() {
		myNetwork.Disconnect ();
		Debug.Log("Just disconnected the network"); 
	}
}
